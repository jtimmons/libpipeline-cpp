// -*- mode:c++ -*-
#pragma once

/**
 *  `Producer` functions and functors to inject data into a pipeline.
*/

#include <memory>
#include <vector>

#include "pipeline.h"
#include "Pipe.h"

/***************************************************************************
 * Vector Producer
 *   Iterates over a vector and outputs the contents to an output pipe. Note
 *   that this should be replaced in the future with a more generic function
 *   that operators on iterators.
 ***************************************************************************/
template <typename T>
std::function<void (std::shared_ptr<Pipe<T>>)>
vector_producer (std::vector<T> in)
{
  return [in](std::shared_ptr<Pipe<T>> out)
  {
    for ( T val : in )
    {
      out->push(val);
    }
    out->close();
  };
}
