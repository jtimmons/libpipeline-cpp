// -*- mode:c++ -*-
#pragma once

#include <thread>
#include <mutex>
#include <memory>
#include <vector>
#include <functional>

#include "Pipe.h"

/***************************************************************************
 * Definition for the core Pipeline class.                                 *
 ***************************************************************************/

/**
 * A pipeline of functions.
 */
template <typename InputType, typename ReturnType>
class Pipeline
{
  using InputPipe = std::shared_ptr< Pipe<InputType> >;
  using ReturnPipe = std::shared_ptr< Pipe<ReturnType> >;
  
public:

  /** Construct a NEW pipeline out of two complex functions.
   *  
   *  The first function is the first in the entire pipeline and so must be a
   *  producer. It only takes the intermediate pipe as the 
   */
  template <typename IntermediateType>
  Pipeline
  ( std::function<void ( std::shared_ptr< Pipe<IntermediateType> > )> f1,
    std::function<void ( std::shared_ptr< Pipe<IntermediateType> > , ReturnPipe)> f2
    )
  {
    mFunction = composite(f1, f2);
  }

  /** Construct an UPDATED pipeline out of an existing pipeline and another function. */
  template <typename IntermediateType>
  Pipeline
  ( Pipeline<InputType, IntermediateType>& p1,
    std::function<void ( std::shared_ptr< Pipe<IntermediateType> > , ReturnPipe)> f2
    )
  {
    // Use the previous pipeline's function + output to connect it to the new pipeline
    std::function<void (std::shared_ptr< Pipe<IntermediateType> >)> f1 =
      [&p1](std::shared_ptr< Pipe<IntermediateType> > pipe) -> void
    {
      p1.run();
      std::shared_ptr<Pipe<IntermediateType>> p1_pipe = p1.getResults();

      while ( ! p1_pipe->isClosed() || ! p1_pipe->empty() )
      {
	pipe->push( p1_pipe->pop() );
      }
      pipe->close();
    };

    mFunction = composite(f1, f2);
  }

  void run()
  {
    mFunction();

    std::unique_lock<std::mutex> lock(this->mThreadsMutex);

    for( std::thread& t : mThreads )
    {
      t.join();
    }

    mThreads.clear();
    lock.unlock();
  }

  /** Gets the results of the computation. */
  ReturnPipe getResults()
  {
    /* TODO: We don't want to be calling this function whenever we need to get
     * results from the queue. Consider having the `run` method return the results
     * of the function somehow (ie. for cases where it doesn't use an output pipe
     * maybe?
     */
    return mOutputPipe;
  }

private:

  /**
   * Composite a producer function with the existing pipeline.
   *
   * Note: This simply wraps the producer in a lambda which throws out the input pipe.
   *       The resulting function is then used in the more general composite case.
   */
  template <typename IntermediateType>
  std::function<void ()> composite
  ( std::function<void (std::shared_ptr< Pipe<IntermediateType> >)> f1,
    std::function<void (std::shared_ptr< Pipe<IntermediateType> >, ReturnPipe)> f2
    )
  {
    std::function<void (InputPipe, std::shared_ptr< Pipe<IntermediateType> >)> new_f1 =
      [f1](InputPipe in, std::shared_ptr< Pipe<IntermediateType> > out)
    {
      f1(out);
    };
    
    return composite(new_f1, f2);
  }

  /**
   * Composite two functions with the existing pipeline.
   */
  template <typename IntermediateType>
  std::function<void ()> composite
  ( std::function<void (InputPipe, std::shared_ptr< Pipe<IntermediateType> >)> f1,
    std::function<void (std::shared_ptr< Pipe<IntermediateType> >, ReturnPipe)> f2
    )
  {
    InputPipe in = mOutputPipe; // Previous output
    std::shared_ptr< Pipe<IntermediateType> > pipe = std::make_shared< Pipe<IntermediateType> >();
    ReturnPipe out = std::make_shared< Pipe<ReturnType> >();

    std::function<void ()> fn = [f1, f2, in, pipe, out, this]() -> void
    {
      std::unique_lock<std::mutex> lock(this->mThreadsMutex);

      this->mThreads.push_back( std::thread(f1, in, pipe) );
      this->mThreads.push_back( std::thread(f2, pipe, out) );

      lock.unlock();
    };

    mOutputPipe = out;

    return fn;
  }

  /** The output pipe for this pipeline.
   *
   *  Without the use of a collector pipe at the end of the pipeline, then
   *  this will just be filled with the output of the final function in the
   *  pipeline.
   */
  ReturnPipe mOutputPipe;

  /** The threads which were used to run the stages in the pipeline. */
  std::vector< std::thread > mThreads;
  std::mutex mThreadsMutex;

  /** The function which is run to run the pipeline */
  std::function<void ()> mFunction;

};
