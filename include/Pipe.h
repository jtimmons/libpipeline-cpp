// -*- mode:c++ -*-
#pragma once

#include <queue>
#include <atomic>
#include <mutex>
#include <condition_variable>

/**
 * A closeable, concurrent blocking queue implementation to act as a thread-safe pipe between functions.
 */
template <typename T>
class Pipe
{

public:
  Pipe()
    : mQueue(),
      mIsClosed(false)
  { }

  /** Closes the queue so that no more data can be added to it. */
  void close()
  {
    mIsClosed = true;
  }

  /** Returns whether the pipe is closed or not. */
  bool isClosed()
  {
    return mIsClosed;
  }

  /** Returns whether the pipe is empty or not. */
  bool empty()
  {
    std::unique_lock<std::mutex> lock(mMutex);
    return mQueue.empty();
  }

  /** Pushes a new element onto the end of the queue. */
  bool push(const T& elem)
  {
    std::unique_lock<std::mutex> lock(mMutex);

    mQueue.push(elem);
    
    lock.unlock();
    mCond.notify_one();
  }

  /** Pops the top element off of the front of the queue.
   *
   *  Note: This is a blocking call. If there is no data in the queue, then it
   *  will block until there is new data to be read off of the queue. Be careful
   *  about calling `pop` before checking if the queue is closed.
   */
  const T& pop()
  {
    std::unique_lock<std::mutex> lock(mMutex);

    while ( mQueue.empty() )
      mCond.wait(lock);

    const T& value = mQueue.front();
    mQueue.pop();

    return value;    
  }

private:
  std::queue<T> mQueue;
  std::mutex mMutex;
  std::condition_variable mCond;

  bool mIsClosed;
};
