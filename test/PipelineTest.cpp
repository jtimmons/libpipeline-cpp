#include <functional>

#include <gtest/gtest.h>

#include <pipeline.h>
#include <Pipe.h>

void producerFn( std::shared_ptr<Pipe<int>> outPipe )
{
  outPipe->push(1);
  outPipe->push(2);
  outPipe->push(3);
  outPipe->push(4);
  outPipe->push(5);

  outPipe->close();
}

void processFn( std::shared_ptr<Pipe<int>> inPipe, std::shared_ptr<Pipe<int>> outPipe)
{
  while ( ! inPipe->isClosed() || ! inPipe->empty() )
  {
    int i = inPipe->pop();
    outPipe->push( i * 2 );
  }
  outPipe->close();
}

void processFn2( std::shared_ptr<Pipe<int>> inPipe, std::shared_ptr<Pipe<int>> outPipe)
{
  while ( ! inPipe->isClosed() || ! inPipe->empty() )
  {
    int i = inPipe->pop();
    outPipe->push( i * 3 );
  }
  outPipe->close();
}

TEST( pipeline_test, pipeline_construction_test )
{
  std::function<void (std::shared_ptr<Pipe<int>>)> f1 = producerFn;
  std::function<void (std::shared_ptr<Pipe<int>>, std::shared_ptr<Pipe<int>>)> f2 = processFn;
  std::function<void (std::shared_ptr<Pipe<int>>, std::shared_ptr<Pipe<int>>)> f3 = processFn2;

  Pipeline<int, int> pipeline(f1, f2);
  Pipeline<int, int> pipeline2(pipeline, f3);
  pipeline2.run();
  std::shared_ptr<Pipe<int>> results = pipeline2.getResults();

  ASSERT_EQ( results->pop(), 6);
  ASSERT_EQ( results->pop(), 12);
  ASSERT_EQ( results->pop(), 18);
  ASSERT_EQ( results->pop(), 24);
  ASSERT_EQ( results->pop(), 30);
}
