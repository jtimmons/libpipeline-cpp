#include <gtest/gtest.h>

#include <Pipe.h>

TEST( pipe_test, single_threaded_mods )
{
  Pipe<int> p;
  p.push(1);
  p.push(2);
  p.push(3);
  p.push(4);
  p.push(5);

  ASSERT_EQ( p.pop(), 1);
  ASSERT_EQ( p.pop(), 2);
  ASSERT_EQ( p.pop(), 3);
  ASSERT_EQ( p.pop(), 4);
  ASSERT_EQ( p.pop(), 5);
}
