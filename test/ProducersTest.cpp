#include <functional>
#include <vector>

#include <gtest/gtest.h>

#include <pipeline.h>
#include <Pipe.h>
#include <functions/producers.h>

void processFn3( std::shared_ptr<Pipe<int>> inPipe, std::shared_ptr<Pipe<int>> outPipe)
{
  while ( ! inPipe->isClosed() || ! inPipe->empty() )
  {
    int i = inPipe->pop();
    outPipe->push( i * 2 );
  }
  outPipe->close();
}

TEST( producers_test, vector_producer_test )
{
  std::vector<int> input {1, 2, 3, 4, 5};
  
  std::function<void (std::shared_ptr<Pipe<int>>, std::shared_ptr<Pipe<int>>)> f1 = processFn3;
  
  Pipeline<int, int> pipeline(vector_producer(input), f1);
  pipeline.run();
  std::shared_ptr<Pipe<int>> results = pipeline.getResults();

  ASSERT_EQ( results->pop(), 2);
  ASSERT_EQ( results->pop(), 4);
  ASSERT_EQ( results->pop(), 6);
  ASSERT_EQ( results->pop(), 8);
  ASSERT_EQ( results->pop(), 10);
}
